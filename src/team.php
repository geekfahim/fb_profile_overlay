<?php
session_start();
require_once __DIR__ . '/Facebook/autoload.php';
$fb = new Facebook\Facebook([
  'app_id' => '208149372945528',
  'app_secret' => '1fdb42f62147d18bf1f66a6afe7afbd7',
  'default_graph_version' => 'v2.8',
  ]);

$helper = $fb->getRedirectLoginHelper();
$permissions = ['email','publish_actions']; // optionnal
try {
  if (isset($_SESSION['facebook_access_token'])) {
	$accessToken = $_SESSION['facebook_access_token'];
	} else {
  		$accessToken = $helper->getAccessToken();
	}
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

if (isset($accessToken)) {
	if (isset($_SESSION['facebook_access_token'])) {
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	} else {
		$_SESSION['facebook_access_token'] = (string) $accessToken;
	  	// OAuth 2.0 client handler
		$oAuth2Client = $fb->getOAuth2Client();
		// Exchanges a short-lived access token for a long-lived one
		$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
		$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	}
	// validating the access token
	



// Logged in
//echo '<h3>Signed Request</h3>';
//var_dump($helper->getSignedRequest());

//echo '<h3>Access Token</h3>';
//var_dump($accessToken->getValue());
?>

<!DOCTYPE html>
<html>
<head>
	<title>
		Team Selection
	</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/respons.css">
	<link rel="stylesheet" href="css/main.css"  type="text/css">
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
</head>
<body>

<div class="main">
	<div class="panel-heading" style="border: 2px solid #3B5998;width: 750px; margin: 0px auto;border-top: 0px;">
	  <h4 class="panel-title" >Choose Your Favourite Team</h4>
	    <div class="team" >
			<a href="https://codefros.com/apps/bplt20/dhaka.php" target="_top"><input class="btn" type="submit" name="submit" value="Dhaka Dynamites"></a>
			<a href="https://codefros.com/apps/bplt20/ctg.php" target="_top"><input class="btn" type="submit" name="submit" value="Chittagong Vikings"></a>
			<a href="https://codefros.com/apps/bplt20/comilla.php" target="_top"><input class="btn" type="submit" name="submit" value="Comilla Victorians"></a>
			<a href="https://codefros.com/apps/bplt20/khulna.php" target="_top"><input class="btn" type="submit" name="submit" value="Khulna Titans"></a>
			<a href="https://codefros.com/apps/bplt20/barishal.php" target="_top"><input class="btn" type="submit" name="submit" value="Barisal Bulls"></a>
			<a href="https://codefros.com/apps/bplt20/rangpur.php" target="_top"><input class="btn" type="submit" name="submit" value="Rangpur Riders"></a>
			<a href="https://codefros.com/apps/bplt20/rajshahi.php" target="_top"><input class="btn" type="submit" name="submit" value="Rajshahi Kings"></a>
		</div>
		 <div class="page" style= "margin-top :15px" ><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fcodefros%2F&tabs&width=340&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1677480285897992" width="340" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></div>
		
	</div>

	</div>
	<center><div>
      <a href="logout.php" style="background: #3E5A98;"><input class="btn" type="submit" name="submit" value="Logout"></a>
    </div></center>
</body>
</html>

<?php

}
else {
	$helper = $fb->getRedirectLoginHelper();
	$loginUrl = $helper->getLoginUrl('https://codefros.com/apps/bplt20/index.php', $permissions);
	echo "<script>window.top.location.href='".$loginUrl."'</script>";
}