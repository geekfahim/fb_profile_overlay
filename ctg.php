<?php
session_start();
require_once __DIR__ . '/Facebook/autoload.php';
$fb = new Facebook\Facebook([
  'app_id' => '208149372945528',
  'app_secret' => '1fdb42f62147d18bf1f66a6afe7afbd7',
  'default_graph_version' => 'v2.8',
]);
$helper = $fb->getRedirectLoginHelper();
$permissions = []; // optionnal
try {
	if (isset($_SESSION['facebook_access_token'])) {
	$accessToken = $_SESSION['facebook_access_token'];
	} else {
  		$accessToken = $helper->getAccessToken();
	}
} catch(Facebook\Exceptions\FacebookResponseException $e) {
 	// When Graph returns an error
 	echo 'Graph returned an error: ' . $e->getMessage();
  	exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
 	// When validation fails or other local issues
	echo 'Facebook SDK returned an error: ' . $e->getMessage();
  	exit;
 }
if (isset($accessToken)) {
	if (isset($_SESSION['facebook_access_token'])) {
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	} else {
		$_SESSION['facebook_access_token'] = (string) $accessToken;
	  	// OAuth 2.0 client handler
		$oAuth2Client = $fb->getOAuth2Client();
		// Exchanges a short-lived access token for a long-lived one
		$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
		$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
		$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
	}
	// validating the access token
	try {
		$request = $fb->get('/me');
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		// When Graph returns an error
		if ($e->getCode() == 190) {
			unset($_SESSION['facebook_access_token']);
			$helper = $fb->getRedirectLoginHelper();
			$loginUrl = $helper->getLoginUrl('https://codefros.com/apps/bplt20/ctg.php/', $permissions);
			echo "<script>window.top.location.href='".$loginUrl."'</script>";
			exit;
		}
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		// When validation fails or other local issues
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}
	// getting basic info about user
	try {
		$requestPicture = $fb->get('/me/picture?redirect=false&width=500&height=500'); //getting user picture
		$requestProfile = $fb->get('/me'); // getting basic info
		$picture = $requestPicture->getGraphUser();
		$profile = $requestProfile->getGraphUser();
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		// When Graph returns an error
		echo 'Graph returned an error: ' . $e->getMessage();
		unset($_SESSION['facebook_access_token']);
		echo "<script>window.top.location.href='https://codefros.com/apps/bplt20/ctg.php/'</script>";
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		// When validation fails or other local issues
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}
	// priting basic info about user on the screen
	//print_r($picture);
	$img = __DIR__.'/img/'.$profile['id'].'.jpg';

	file_put_contents($img, file_get_contents($picture['url']));
  	// Now you can redirect to another page and use the access token from $_SESSION['facebook_access_token']


try {
	$stamp = imagecreatefrompng('ctg.png');
$im = imagecreatefromjpeg($img);

// Set the margins for the stamp and get the height/width of the stamp image
$marge_right = 0;
$marge_bottom = 0;
$sx = imagesx($stamp);
$sy = imagesy($stamp);

// Copy the stamp image onto our photo using the margin offsets and the photo 
// width to calculate positioning of the stamp. 
imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

// Output and free memory
//header('Content-type: image/png');
imagepng($im,'img/'.$profile['id'].'.png');
imagedestroy($im);
unlink($img);

}
 catch(Exception $e) {
    echo 'Error: ' . $e->getMessage();
}

	//echo 'name' .' '. $profile['name'];
if (isset($_POST['submit'])) {


	
	$permissions = $fb->get('/me/permissions');
	$permissions = $permissions->getGraphEdge()->asArray();
	// printing declined and granted permission
	//echo "<pre>";
	//print_r($permissions);
	//echo "</pre>";
	
	foreach ($permissions as $key) {
		if ($key['status'] == 'declined') {
			$declined[] = $key['permission'];
			$loginUrl = $helper->getLoginUrl('https://codefros.com/apps/bplt20/ctg.php', $declined);
			echo "<script>window.top.location.href='".$loginUrl."'</script>";
		}
	}

			try {
		// message must come from the user-end
		$data = ['source' => $fb->fileToUpload(__DIR__.'/img/'.$profile['id'].'.png'), 'message' => $_POST['message']];
		$request = $fb->post('/me/photos', $data);
		$response = $request->getGraphNode()->asArray();

		try {
		$post = $fb->post('/'.$response['id'].'/comments', array('message' => ''));
		$post = $post->getGraphNode()->asArray();
		
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		// When Graph returns an error
		//echo 'Graph returned an error: ' . $e->getMessage();
		session_destroy();
		//exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		// When validation fails or other local issues
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}
	
	

		echo "<script>window.top.location.href='https://facebook.com/photo.php?fbid=".$response['id']."&makeprofile=1'</script>";
		

	} catch(Facebook\Exceptions\FacebookResponseException $e) {
		// When Graph returns an error
		echo 'Graph returned an error: ' . $e->getMessage();
		exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
		// When validation fails or other local issues
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}
	//echo $response['id'];
	//$img1 = __DIR__.'/img/'.$profile['id'].'.png';
	//unlink($img1);


	}
	


	?>


<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BPL-T20</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="css/main.css"  type="text/css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/skeleton.css">
    <link rel="icon" type="image/png" href="images/favicon.png">
  </head> 
  <body>
 <div class="row" style="border: 2px solid #3B5998;width: 750px; margin: 0px auto;margin-top: 0px";>
        <div class="panel panel-danger">
			  <div class="panel-heading">
				<h2 class="panel-title">Support Your Favourite Team</h2>
			  </div>
	  <div class="panel-body" style="padding:15px">
		  
		<div class="omb_login">
	  
		<div class="row omb_row-sm-offset-3">
			<div class="col-xs-12 col-sm-6">	

					<div class="profile"><?php echo '<img style="width:260px;height:280px;" src="img/'.$profile['id'].'.png"/>';?>	
			
    <form action="" method='POST'>
		  <textarea class="u-full-width" placeholder="Write something..." name="message" type="text"></textarea>
		  <input class="btn" value="Post to FACEBOOK" type="submit" name="submit">
		</form>
		
				<p>Created By <a href="https://facebook.com/codefros" target="_blank"  style="text-decoration: none;color: #6BD62D"><b>Codefros</a></p>
		</div>
		<div>
      <!--<a href="logout.php"><input class="btn" type="submit" name="submit" value="Logout"></a>-->
    </div>
    	</div>
		</div>
		
	</div>
	
	
	</div>
	
 </div>
</body>
</html>

	<?php


} 
else {
	$helper = $fb->getRedirectLoginHelper();
	$loginUrl = $helper->getLoginUrl('https://codefros.com/apps/bplt20/ctg.php/', $permissions);
	echo "<script>window.top.location.href='".$loginUrl."'</script>";
}